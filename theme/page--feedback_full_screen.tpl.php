<?php

/**
 * @file
 * Theme implementation to display fullscreen page.
 */
?>
<?php if ($title) : ?>
  <h1><?php print $title; ?></h1>
<?php endif; ?>
<?php if ($messages): ?>
  <?php print $messages; ?>
<?php endif; ?>
<div class="wrapper">
  <?php print render($page['content']); ?>
</div>
