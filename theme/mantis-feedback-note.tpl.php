<?php
/**
 * @file
 * Theme implementation to display a Mantis comment.
 *
 * Available variables:
 *
 * $id (integer) - Mantis issue ID
 * $reporter (object) -  Mantis reporter
 * $date_submitted (integer) - UNIX timestamp of the creation date.
 * $last_modified (integer) - UNIX timestamp of the last modified date.
 * $view_state (string) -
 * $text (string) - The text of the comment.
 */
?>
<div class="note <?php print $classes; ?>">
  <header class="submitted">
    <span class="issue-id"><?php print $id; ?></span>|
    <span class="issue-reporter"><?php print $reporter->real_name; ?></span>|
    <span class="issue-last_modified"><?php print format_date($last_modified); ?></span>
    <a href="#" class="trigger"><?php print $last ? t('hide') : t('open') ?></a>
  </header>
  <div class="text">
    <?php print $text; ?>
  </div>
</div>
