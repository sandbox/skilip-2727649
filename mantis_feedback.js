(function($) {

  /**
   * Custom callback triggered bij the SVG image link 'link-bug.svg'.
   * Used to open or close the push menu.
   */
  window.pushMenuToggle = function(a) {
    $('body').addClass('push-body');
    $('#mantis-feedback-wrapper').toggleClass('menu-open');
    $('body').toggleClass('push-body-toleft');
  };

  /**
   * Overwrite for the Form review function of the Feedback plugin.
   *
   * Used to print out the value of the form elements. Normally this will output
   * the string length of the user input value.
   */
  window.Feedback.Form.prototype.review = function(dom) {
    var i = 0, item, len = this.elements.length;

    for (; i < len; i++) {
      item = this.elements[i];

      if (item.element.value.length > 0) {
        var label = document.createElement('label');
        label.appendChild(document.createTextNode(item.name + ':'));
        dom.appendChild(label);
        dom.appendChild(document.createTextNode(item.element.value));
        dom.appendChild(document.createElement('hr'));
      };
    };
    return dom;
  };

  /**
   * Overwrite for the XHR send function of the Feedback plugin.
   *
   * Used to trigger the YSStickyIssuesList event to refresh the list of issues
   * after succesfully creating a new issue using the Mantis API.
   * Also used to pass extra information to the AJAX callback function.
   */
  window.Feedback.XHR.prototype.send = function(data, callback) {
    var xhr = this.xhr;

    xhr.onreadystatechange = function() {
      if (xhr.readyState == 4) {
        callback((xhr.status === 200));

        if (xhr.status === 200) {
          $('#mantis-feedback-wrapper').trigger('YSStickyIssuesList');
        };
      };
    };
    data[0].url = window.location.href;
    data[0].browser = $.browser;

    xhr.open("POST", this.url, true);
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhr.send("data=" + encodeURIComponent(window.JSON.stringify(data)));
  };

  /**
   * Create the issue list using Drupal's AJAX framework.
   */
  $(function() {
    if (!Drupal.settings.mantis_feedback || !Drupal.settings.mantis_feedback.list_url) {
      return;
    };

    // Add the toggle-collapse behavior to the open/close button.
    $('#push-menu-toggle').bind('click', function(e) {
      e.preventDefault();
      window.pushMenuToggle();
    });

    var ajax_settings = {};
    ajax_settings.event = 'YSStickyIssuesList';
    ajax_settings.url = Drupal.settings.mantis_feedback.list_url + '?url=' + encodeURIComponent(window.location.href);
    ajax_settings.progress = {type: 'none'};

    Drupal.ajax['mantis-feedback-issue-list'] = new Drupal.ajax('mantis-feedback-issue-list', $('#mantis-feedback-wrapper')[0], ajax_settings);

    Drupal.ajax['mantis-feedback-issue-list'].options.success = function(response, status) {
      if (typeof response == 'string') {
        response = $.parseJSON(response);
      };

      // Call the ajax success method.
      Drupal.ajax['mantis-feedback-issue-list'].success(response, status);

      // Each time the sidebar recieved it's issues, the Feedback button should
      // be added to the sidebar again.
      if (Drupal.settings.mantis_feedback.loggedin) {
        Feedback({
          url: Drupal.settings.mantis_feedback.add_url,
          'appendTo': $('#mantis-feedback-wrapper').get(0),
          'label': Drupal.t('Report new issue'),
          'header': Drupal.t('New issue'),
          'screenshotDescription': Drupal.t('Next'),
              'pages': [
              // These are the pages used in the Feedback workflow.
              // The form can be extendes with other form elements.
              new window.Feedback.Form([{
                type: 'input',
                attributes: {
                  'type': 'text',
                  'placeholder': Drupal.t('Enter a short description')
                },
                name: 'summary',
                label: Drupal.t('Summary'),
                required: false
              },
              {
                type: "textarea",
                attributes: {'placeholder': Drupal.t('Give a description of the problem or comment here')},
                name: "description",
                label: Drupal.t('Description'),
                required: false
              }]),
              new window.Feedback.Screenshot(),
              new window.Feedback.Review()
          ]
        });
      }

      // If the Feedback modal is opened, the sidebar menu must be closed for a
      // proper screenshot.
      $('.feedback-bottom-right').bind('click', function() {
        $('#mantis-feedback-wrapper').toggleClass('menu-open');
        $('body').toggleClass('push-body-toleft');
        $('body').removeClass('push-body');
      });

      // Set focus in the search field.
      //$('#ckeditor-file-browser-dialog .linkit-search-element').focus();
    };
    $('#mantis-feedback-wrapper').trigger('YSStickyIssuesList');

    /**
     * Close the Feedback window when pressing the escape key.
     */
    $(document).keyup(function(e) {
      if (e.keyCode == 27) {
        $('.feedback-close').trigger('click');
      };
    });

  });

  /**
   *
   */
  Drupal.behaviors.feedbackModal = {
    attach: function(context, settings) {
      $('.modal-popup').bind('click', function (e) {
        e.preventDefault();
        $new_content = '<div><iframe class="modal-iframe" src="' + $(this).attr('href') + '"></iframe></div>';

        var title = ($(this).hasClass('no-title') ? false : $(this).text());

        $('body').append(Drupal.theme('feedbackOverlay', $new_content, title));
        $('body').find('.feedback-modal').css('max-height', ($(window).height() - 100));
        Drupal.attachBehaviors('.feedback-modal');
      });
    }
  };

  /**
   *
   */
  Drupal.behaviors.feedbackModalClose = {
    attach: function(context, settings) {
      $('.mantis-feedback-close').bind('click', function (e) {
        e.preventDefault();
        $('.feedback-modal').remove();
        $('.feedback-glass').remove();
      });
      }
    }

  /**
   * Theme status messages.
   */
  Drupal.theme.prototype.feedbackOverlay = function(content, title) {
    var output = '<div class="feedback-glass"></div>';
    output += '<div class="feedback-modal custom-modal' + (title ? '' : ' no-title') + '" style="max-height:100%;">';
    output += (title ? '<div class="feedback-header">' : '');
    output += '<a class="mantis-feedback-close feedback-close" href="#">×</a>';
    output += (title ? '<h3>' + title + '</h3>' : '');
    output += (title ? '</div>' : '');
    output += '<div class="feedback-body mantis-feedback-body">';
    output += content;
    output += '</div>';
    output += '</div>';
    return output;
  };

  Drupal.behaviors.niceSelects = {
    attach: function(context, settings) {
      $('.feedback-full-screen select').once('custom-select').each(function() {
        var $_this = $(this);
        var $custom_select = $('<div/>').addClass('custom-select').text(getSelectedOptionText($_this)).append(
          $('<span/>')
          .html('<span class="indicator">▾</span>')
          .addClass('select-button')
        );
        $_this
          .css({
            'position': 'absolute',
            'z-index': 1,
            'opacity': 0
          })
          .after($custom_select)
          .bind('change', function(){
            $custom_select.text(getSelectedOptionText($_this));
            $('#mantis-feedback-form').submit();
          });
      });
    }
  }

  var getSelectedOptionText = function($element) {
    if ($element.length > 0) {
      try {
        return $($element.get(0).options[$element.get(0).selectedIndex]).text();
      }
      catch(err) {
        return $($element.get(0).options[$element.eq(0).selectedIndex]).text();
      }
    }
  }

  Drupal.behaviors.collapsibleNotes = {
    attach: function(context, settings) {
      $('.note').once('collapse').each(function() {
        var $wrapper = $(this);

        $wrapper.find('.trigger').click(function (e) {
          e.preventDefault();
          $trigger = $(this);
          //getting the next element
          $content = $wrapper.find('.text');
          $content.toggleExpand();
        });
      });
    }
  };

  Drupal.behaviors.reloadListLoggedin = {
    attach: function(context, settings) {
      $('#mantis-feedback-login-form').ajaxComplete(function(event, xhr, settings) {
        if (Drupal.settings.mantis_feedback.loggedin) {
          $('#mantis-feedback-wrapper').trigger('YSStickyIssuesList');
        }
      });
      $('#mantis-feedback-logout-form').ajaxComplete(function(event, xhr, settings) {
        if (!Drupal.settings.mantis_feedback.loggedin) {
          $('#mantis-feedback-wrapper').trigger('YSStickyIssuesList');
        }
      });
    }
  }
  /**
   *
   */
  Drupal.behaviors.collapseAll = {
    attach: function(context, settings) {
      $('.expand-btn').once('expand-btn').each(function() {
        $(this).bind('click', function(e) {
          e.preventDefault();
          $trigger = $(this);
          var open = $('.expand-btn').hasClass('expanded') ? true : false;
          $('.note').each(function() {
            var $content = $(this).find('.text');
            if (!open) {
              $('.expand-btn').text(Drupal.t('collapse all'));
              $content.toggleExpand('slideDown');
              $('.expand-btn').addClass('expanded');
            }
            else {
              $('.expand-btn').text(Drupal.t('expand all'));
              $content.toggleExpand('slideUp');
              $('.expand-btn').removeClass('expanded');
            }
          });
        });
      });
    }
  }
  /**
   *
   */
  Drupal.behaviors.historyDisplay = {
    attach: function(context, settings) {
      $('.history-btn').once('expand-btn').each(function() {
        $(this).bind('click', function(e) {
          e.preventDefault();
          $trigger = $(this);
          if (!$trigger.hasClass('expanded')) {
            $('.note').fadeIn();
            $trigger.text(Drupal.t('hide history')).addClass('expanded');
          }
          else {
            $('.note:not(.last)').fadeOut();
            $trigger.text(Drupal.t('show history')).removeClass('expanded');
          }
        });
      });
    }
  }
  /**
   *
   */
  $.fn.toggleExpand = function(fn_name) {
    var animation_type = fn_name || 'slideToggle';
    var $content = $(this);
    var $wrapper = $content.parents('.note');
    var $trigger = $wrapper.find('.trigger');

    // Open up the content needed - toggle the slide- if visible, slide up, if not slidedown.
    $content[animation_type](100, function () {
      //execute this after slideToggle is done
      //change text of header based on visibility of content div
      $trigger.text(function () {
        //change text based on condition
        return $content.is(':visible') ? Drupal.t('hide') : Drupal.t('open');
      });
      if (!$wrapper.hasClass('expanded')) {
        $wrapper.addClass('expanded');
      }
      else {
        $wrapper.removeClass('expanded');
      }
    });
  };

  /**
   *
   */
/*
  Drupal.behaviors.titleDisplay = {
    attach: function(context, settings) {
      $('.custom-modal').once('display-title').each(function() {
        $wrapper = $(this);
        $wrapper.find('.feedback-header').bind('click', function(e) {
          $(this).hide();
          var $title_field = $('.modal-iframe').contents().find('.form-item-issue-summary input');
          $title_field.show();
          $title_field.parent('.form-item').addClass('shown');
          $('.modal-iframe').contents().find('body').addClass('title-textfield-shown');
          $title_field.focus();

          $title_field.bind('blur', function () {
            $wrapper.find('.feedback-header h3').text($(this).val());
            $title_field.hide();
            //var $title_field = $('.modal-iframe').contents().find('#edit-issue-summary');
            //$title_field.focus();
            //$wrapper.find('.feedback-header').show();
            //$title_field.parent('.form-item').removeClass('shown');
            //$('.modal-iframe').contents().find('body').removeClass('title-textfield-shown');
          });
        });
      });
    }
  }
*/

})(jQuery);
