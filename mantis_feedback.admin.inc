<?php
/**
 * @file
 * mantis bugtracker
 */

/**
 * Mantis_feedback_settings_form().
 */
function mantis_feedback_settings_form() {
  $form = array();
  $form['mantis_feedback_api_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Mantis API settings'),
    '#collapsible' => TRUE,
  );
  $form['mantis_feedback_api_settings']['mantis_feedback_api_url'] = array(
    '#type' => 'textfield',
    '#title' => t('API url'),
    '#default_value' => variable_get('mantis_feedback_api_url', ''),
    '#required' => TRUE,
  );
  $form['mantis_feedback_api_settings']['mantis_feedback_api_project_id'] = array(
    '#type' => 'textfield',
    '#title' => t('API project id'),
    '#default_value' => variable_get('mantis_feedback_api_project_id', ''),
    '#required' => TRUE,
  );

  $form['mantis_feedback_site_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Site settings'),
    '#collapsible' => TRUE,
  );
  $form['mantis_feedback_site_settings']['mantis_feedback_project_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Project name'),
    '#default_value' => variable_get('mantis_feedback_project_name', ''),
  );
  $form['mantis_feedback_site_settings']['mantis_feedback_logo'] = array(
    '#type' => 'managed_file',
    '#title' => t('Logo'),
    '#default_value' => variable_get('mantis_feedback_logo', ''),
    '#upload_location' => 'public://sticky_issues/',
  );

  return system_settings_form($form);
}
